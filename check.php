<?php
  include 'check_string.php';

	session_start();
  $_SESSION['form_value'] = $_POST;

  $check_string = new check_string($_POST);

  $_SESSION['error_message'] = $check_string->allocate_post();

?>

<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>会員登録</title>
  <link href="./style.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="script.js"></script>
</head>
<body>
  <div id="all">
  	<h1>入力確認</h1>
  	<p>入力した内容でよろしいでしょうか？</p>
  	<dl>
      <dt>
        お名前
      </dt>
      <dd>
    		姓：<?php if(!empty($_POST['lastname'])){
    								print htmlspecialchars($_POST['lastname'],ENT_QUOTES,'UTF-8');
    							}else{
    								$_SESSION['error_message']['lastname'] = "姓を入力してください";
    							}
    				?> <br>
    		名：<?php if(!empty($_POST['firstname'])){
    								print htmlspecialchars($_POST['firstname'],ENT_QUOTES,'UTF-8');
    							}else{
    								$_SESSION['error_message']['firstname'] = "名を入力してください。";
    							}
    				?>
      </dd>
    </dl>
    <dl>
      <dt>
      	ふりがな
      </dt>
      <dd>
    		せい：<?php if(!empty($_POST['ln_rb'])){
    								print htmlspecialchars($_POST['ln_rb'],ENT_QUOTES,'UTF-8');
    							}else{
    								$_SESSION['error_message']['ln_rb'] = "姓のふりがなを入力してください。";
    							}
    					?> <br>
    		めい：<?php if(!empty($_POST['fn_rb'])){
    								print htmlspecialchars($_POST['fn_rb'],ENT_QUOTES,'UTF-8');
    							}else{
    								$_SESSION['error_message']['fn_rb'] = "名のふりがなを入力してください。";
    							}
    				?>
      </dd>
    </dl>
    <dl>
      <dt>
      	ニックネーム
      </dt>
      <dd>
  			<?php if(!empty($_POST['nickname'])){
  							print htmlspecialchars($_POST['nickname'],ENT_QUOTES,'UTF-8');
  						}else{
  							$_SESSION['error_message']['nickname'] = "ニックネームを入力してください。";
  						}
  			?>
      </dd>
    </dl>
    <dl>
      <dt>
        性別
      </dt>
      <dd>
    		<?php if(!empty($_POST['sei'])){
  							print htmlspecialchars($_POST['sei'],ENT_QUOTES,'UTF-8');
  						}else{
  							$_SESSION['error_message']['sei'] = "性別を選んでください。";
  						}
  			?>
      </dd>
    </dl>
    <dl>
      <dt>
        メールアドレス
      </dt>
      <dd>
        <?php if(!empty($_POST['mail'])){
  							print htmlspecialchars($_POST['mail'],ENT_QUOTES,'UTF-8');
  						}else{
  							$_SESSION['error_message']['mail'] = "メールアドレスを入力してください。";
  						}
  			?>
      </dd>
    </dl>
    <dl>
      <dt>
        パスワード
      </dt>
      <dd>
        <?php 
        	if(!empty($_POST['pass'])){
        		$num = strlen($_POST['pass']);
        		if($num < 6){
        			$_SESSION['error_message']['pass'] = "パスワードが短すぎます。";
        		}else{
        			for($i = 0; $i < $num; $i++)
        				print("*");
        		}
        	}else{
        		$_SESSION['error_message']['pass'] = "パスワードを入力してください。";
        	}
        ?>
      </dd>
    </dl>
    <dl>
      <dt>
        パスワード確認
      </dt>
      <dd>
  			<?php 
        	if(!empty($_POST['pass_conf'])){
        		$num = strlen($_POST['pass_conf']);
        		if($_POST['pass'] == $_POST['pass_conf']){
        			for($i = 0; $i < $num; $i++)
        				print("*");
        		}else{
        			$_SESSION['error_message']['pass_conf'] = "パスワードが一致しません";
        		}
        	}else{
        		$_SESSION['error_message']['pass_conf'] = "パスワードが一致しません";
        	}
        ?>      
      </dd>
    </dl>
    <dl>
      <dt>
        郵便番号
      </dt>
      <dd>
        <?php 
        	if(!empty($_POST['postcode'])){
        		print htmlspecialchars($_POST['postcode'],ENT_QUOTES,'UTF-8');
        	}else{
        		$_SESSION['error_message']['postcode'] = "郵便番号を入力してください。";
        	}
        ?>
      </dd>
    </dl>
    <dl>
      <dt>
        都道府県
      </dt>
      <dd>
      	<?php 
        	if(!empty($_POST['prefecture'])){
        		print htmlspecialchars($_POST['prefecture'],ENT_QUOTES,'UTF-8');
        	}else{
        		$_SESSION['error_message']['prefecture'] = "都道府県を入力してください。";
        	}
        ?>
      </dd>
    </dl>
    <dl>
      <dt>
        住所
      </dt>
      <dd>
        <?php 
        	if(!empty($_POST['address'])){
        		print htmlspecialchars($_POST['address'],ENT_QUOTES,'UTF-8');
        	}else{
        		$_SESSION['error_message']['address'] = "住所を入力してください。";
        	}
        ?>
      </dd>
    </dl>
    <dl>
      <dt>
        住所（建物名）
      </dt>
      <dd>
        <?php 
        	if(!empty($_POST['building'])){
        		print htmlspecialchars($_POST['building'],ENT_QUOTES,'UTF-8');
        	}else{
            print "住所（建物名）なし";
          }

        ?>
      </dd>
    </dl>
    <dl>
      <dt>
        電話番号
      </dt>
      <dd>
      	<?php 
        	if(!empty($_POST['phonenumber'])){
        		print htmlspecialchars($_POST['phonenumber'],ENT_QUOTES,'UTF-8');
        	}else{
            print "電話番号なし";
          }
        ?>
      </dd>
    </dl>
    <dl>
      <dt>
   			メッセージ
  	  </dt>
      <dd>
        <?php 
        	if(!empty($_POST['message']) || !($_POST['message'] == "")){
        		print htmlspecialchars($_POST['message'],ENT_QUOTES,'UTF-8');
        	}else{
        		print "メッセージなし";
        	}
        ?>
      </dd>
    </dl>
  	<dl>
      <dt>
        運営からのお知らせ
      </dt>
      <dd>
      	<?php 
        	if(!empty($_POST['user_wish_mail_magazine'])){
        		if($_POST['user_wish_mail_magazine'] == "1"){
        			print "受信する";
        		}else if($_POST['user_wish_mail_magazine'] == "0"){
        			print "受信しない";
        		}
        	}else if($_POST['user_wish_mail_magazine'] == "0"){
              print "受信しない";
          }
        ?>
    	</dd>
  	</dl>

  	<?php 
  	  echo '<a href="' . $_SERVER['HTTP_REFERER'] . '">' . "修正する" . "</a>";
  	?>

  	<a href="complete.php" >確認</a>
  </div>
</body>

<?php
	if(!empty($_SESSION['error_message'])){
  	header('Location: http://192.168.20.125/kadai/index.php');
  }
?>