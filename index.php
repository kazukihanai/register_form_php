<?php
  session_start();

  $lastname_flag = true;
  $firstname_flag = true;
  $ln_rb_flag = true;
  $fn_rb_flag = true;
  $nickname_flag = true;
  $sei_flag = true;
  $sei_flag = true;
  $mail_flag = true;
  $password_flag = true;
  $pass_conf_flag = true;
  $postcode_flag = true;
  $prefecture_flag = true;
  $address_flag = true;
  $building_flag = true;
  $phonenumber_flag = true;

  if(isset($_SESSION['form_value'])) $back_flag = true;
  else $back_flag = false;

  if(isset($_SESSION['error_message'])){

    if(isset($_SESSION['error_message']['lastname'])){
      $lastname_flag = false;
    }
    
    if(isset($_SESSION['error_message']['firstname'])){
      $firstname_flag = false;
    }

    if(isset($_SESSION['error_message']['ln_rb'])){
      $ln_rb_flag = false;
    }
    if(isset($_SESSION['error_message']['fn_rb'])){
      $fn_rb_flag = false;
    }
    if(isset($_SESSION['error_message']['nickname'])){
      $nickname_flag = false;
    }
    if(isset($_SESSION['error_message']['sei'])){
      $sei_flag = false;
    }
    if(isset($_SESSION['error_message']['mail'])){
      $mail_flag = false;
    }
    if(isset($_SESSION['error_message']['pass'])){
      $password_flag = false;
    }
    if(isset($_SESSION['error_message']['pass_conf'])){
      $pass_conf_flag = false;
    }
    if(isset($_SESSION['error_message']['postcode'])){
      $postcode_flag = false;
    }
    if(isset($_SESSION['error_message']['prefecture'])){
      $prefecture_flag = false;
    }
    if(isset($_SESSION['error_message']['address'])){
      $address_flag = false;
    }
    if(isset($_SESSION['error_message']['building'])){
      $building_flag = false;
    }
    if(isset($_SESSION['error_message']['phonenumber'])){
      $phonenumber_flag = false;
    }
  }

  function error_message($message){
    print"<p id=\"errormessage\">$message</p>";
  }

  function form_value_setter($value){
    if(isset($value)){
      print $value;
    }else{
      print "";
    }
  }

  function lastname_setter(){
    global $back_flag;
    if($back_flag)form_value_setter($_SESSION['form_value']['lastname']);
  }

  function lastname_error_message(){
    global $lastname_flag;
    if(isset($_SESSION['error_message'])){
      if(!$lastname_flag) error_message($_SESSION['error_message']['lastname']);
    }
  }

  function firstname_setter(){
    global $back_flag;
    if($back_flag)form_value_setter($_SESSION['form_value']['firstname']);
  }

  function firstname_error_message(){
    global $firstname_flag;
    if(isset($_SESSION['error_message'])){
      if(!$firstname_flag) error_message($_SESSION['error_message']['firstname']);
    }
  }

  function ln_rb_setter(){
    global $back_flag;
    if($back_flag)form_value_setter($_SESSION['form_value']['ln_rb']);
  }


  function ln_rb_error_message(){
    global $ln_rb_flag;
    if(isset($_SESSION['error_message'])){
      if(!$ln_rb_flag) error_message($_SESSION['error_message']['ln_rb']);
    }
  }

  function fn_rb_setter(){
    global $back_flag;
    if($back_flag)form_value_setter($_SESSION['form_value']['fn_rb']);
  }

  function fn_rb_error_message(){
    global $fn_rb_flag;
    if(isset($_SESSION['error_message'])){
      if(!$fn_rb_flag) error_message($_SESSION['error_message']['fn_rb']);
    }
  }

  function nickname_setter(){
    global $back_flag;
    if($back_flag)form_value_setter($_SESSION['form_value']['nickname']);
  }

  function nickname_error_message(){
    global $nickname_flag;
    if(isset($_SESSION['error_message'])){
      if(!$nickname_flag) error_message($_SESSION['error_message']['nickname']);
    }
  }

  function sei_setter(){
    global $back_flag;

  }

  function sei_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function mail_setter(){
    global $back_flag;

  }

  function mail_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function pass_setter(){
    global $back_flag;

  }

  function pass_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function pass_conf_setter(){
    global $back_flag;

  }

  function pass_conf_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function postcode_setter(){
    global $back_flag;

  }

  function postcode_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function prefecture_setter(){
    global $back_flag;

  }

  function prefecture_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function address_setter(){
    global $back_flag;

  }

  function address_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function building_setter(){
    global $back_flag;

  }

  function building_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function phonenumber_setter(){
    global $back_flag;

  }

  function phonenumber_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function message_setter(){
    global $back_flag;

  }

  function message_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }

  function user_wish_mail_magazine_setter(){
    global $back_flag;

  }

  function user_wish_mail_magazine_error_message(){
    if(isset($_SESSION['error_message'])){

    }
  }
?>
<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>会員登録</title>
  <link href="./style.css" rel="stylesheet" type="text/css">
  <script src="https://zipaddr.github.io/zipaddrx.js" charset="UTF-8"></script>
  <script type="text/javascript" src="script.js"></script>
</head>
<body>
  <div id="all">
    <h1>会員登録</h1>
    <h2>会員登録フォーム</h2>
    <form action="check.php" method="post">
      <dl>
        <dt>
          <label for="name">お名前</label> <em>必須</em>
        </dt>
        <dd>
          姓&nbsp;&nbsp;<input name="lastname" type="text" value="<?php lastname_setter() ?>">
            <?php lastname_error_message() ?>
          名&nbsp;&nbsp;<input name="firstname" type="text" value="<?php firstname_setter() ?>">
            <?php firstname_error_message() ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="ruby">ふりがな</label> <em>必須</em>
        </dt>
        <dd>
          せい&nbsp;&nbsp;<input name="ln_rb" placeholder="" type="text" value="<?php ln_rb_setter() ?>">&nbsp;&nbsp;
            <?php ln_rb_error_message() ?>
          めい&nbsp;&nbsp;<input name="fn_rb" placeholder="" type="text" value=" <?php fn_rb_setter() ?>">
            <?php fn_rb_error_message() ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="nickname">ニックネーム</label> <em>任意</em>
        </dt>
        <dd>
          <input name="nickname" type="text" value="<?php nickname_setter() ?>">
            <?php nickname_error_message() ?>
        </dd>
      </dl>
      <dl>
        <dt>
          性別 <em>必須</em>
        </dt>
        <dd>
          <?php
            if($back_flag){
              if($_SESSION['form_value']['sei'] == "男性"){
                print('<input checked="checked" name="sei" type="radio" value="男性">男性</label><input name="sei" type="radio" value="女性">女性</label>');
              }else{
                print('<input name="sei" type="radio" value="男性">男性</label><input checked="checked" name="sei" type="radio" value="女性">女性</label>');
              }
            }else{
              print('<input checked="checked" name="sei" type="radio" value="男性">男性</label><input name="sei" type="radio" value="女性">女性</label>');
            }
            if(!$sei_flag) error_message($_SESSION['error_message']['sei']);
          ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="email">メールアドレス</label> <em>必須</em>
        </dt>
        <dd>
          <input name="mail" type="text" value="<?php if($back_flag)form_value_setter($_SESSION['form_value']['mail']) ?>">
            <?php if(!$mail_flag) error_message($_SESSION['error_message']['mail']); ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="password">パスワード</label> <em>必須</em>
        </dt>
        <dd>
          <input name="pass" type="password" value="<?php if($back_flag)form_value_setter($_SESSION['form_value']['pass']) ?>">
          半角英数字6文字以上を設定してください。
            <?php if(!$password_flag) error_message($_SESSION['error_message']['pass']); ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="password">パスワード(確認)</label> <em>必須</em>
        </dt>
        <dd>
          <input name="pass_conf" type="password" value="<?php if($back_flag)form_value_setter($_SESSION['form_value']['pass_conf']) ?>">
            <?php if(!$pass_conf_flag) error_message($_SESSION['error_message']['pass_conf']); ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="pcode1">郵便番号</label> <em>必須</em>
        </dt>
        <dd>
          <input type="text" id="zip" name="postcode" value="<?php if($back_flag)form_value_setter($_SESSION['form_value']['postcode']) ?>">
            <?php if(!$postcode_flag) error_message($_SESSION['error_message']['postcode']); ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="address">都道府県</label> <em>必須</em>
        </dt>
        <dd>
        <select id="pref" name="prefecture">

        <?php
          if($back_flag){
            print "<option value=\"".$_SESSION['form_value']['prefecture']."\" selected>".$_SESSION['form_value']['prefecture']."</option> ";
          }else print "<option value=\"未選択\" selected>（未選択）</option> ";

        ?>
          
          <option value="北海道">北海道</option>
          <option value="青森県">青森県</option>
          <option value="秋田県">秋田県</option>
          <option value="岩手県">岩手県</option>
          <option value="山形県">山形県</option>
          <option value="宮城県">宮城県</option>
          <option value="福島県">福島県</option>
          <option value="茨城県">茨城県</option>
          <option value="栃木県">栃木県</option>
          <option value="群馬県">群馬県</option>
          <option value="埼玉県">埼玉県</option>
          <option value="神奈川県">神奈川県</option>
          <option value="千葉県">千葉県</option>
          <option value="東京都">東京都</option>
          <option value="山梨県">山梨県</option>
          <option value="長野県">長野県</option>
          <option value="新潟県">新潟県</option>
          <option value="富山県">富山県</option>
          <option value="石川県">石川県</option>
          <option value="福井県">福井県</option>
          <option value="岐阜県">岐阜県</option>
          <option value="静岡県">静岡県</option>
          <option value="愛知県">愛知県</option>
          <option value="三重県">三重県</option>
          <option value="滋賀県">滋賀県</option>
          <option value="京都府">京都府</option>
          <option value="大阪府">大阪府</option>
          <option value="兵庫県">兵庫県</option>
          <option value="奈良県">奈良県</option>
          <option value="和歌山県">和歌山県</option>
          <option value="鳥取県">鳥取県</option>
          <option value="島根県">島根県</option>
          <option value="岡山県">岡山県</option>
          <option value="広島県">広島県</option>
          <option value="山口県">山口県</option>
          <option value="徳島県">徳島県</option>
          <option value="香川県">香川県</option>
          <option value="愛媛県">愛媛県</option>
          <option value="高知県">高知県</option>
          <option value="福岡県">福岡県</option>
          <option value="佐賀県">佐賀県</option>
          <option value="長崎県">長崎県</option>
          <option value="熊本県">熊本県</option>
          <option value="大分県">大分県</option>
          <option value="宮崎県">宮崎県</option>
          <option value="鹿児島県">鹿児島県</option>
          <option value="沖縄県">沖縄県</option>
        </select><br />
          <?php if(!$prefecture_flag) error_message($_SESSION['error_message']['prefecture']); ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="address">住所</label> <em>必須</em>
        </dt>
        <dd>
          <input type="text" id="addr" name="address" value="<?php if($back_flag)form_value_setter($_SESSION['form_value']['address']) ?>">
            <?php if(!$address_flag) error_message($_SESSION['error_message']['address']); ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="address">住所（建物名）</label> <em>任意</em>
        </dt>
        <dd>
          <input type="text" name="building" value="<?php if($back_flag)form_value_setter($_SESSION['form_value']['building']) ?>">
            <?php if(!$building_flag) error_message($_SESSION['error_message']['building']); ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="tel">電話番号</label> <em>任意</em>
        </dt>
        <dd>
          <input type="text" name="phonenumber" value="<?php if($back_flag)form_value_setter($_SESSION['form_value']['phonenumber']) ?>">
            <?php if(!$phonenumber_flag) error_message($_SESSION['error_message']['phonenumber']); ?>
        </dd>
      </dl>
      <dl>
        <dt>
          <label for="tel">メッセージ</label> <em>任意</em>
        </dt>
        <dd>
          <textarea name="message"><?php if($back_flag)form_value_setter($_SESSION['form_value']['message']) ?></textarea>
        </dd>
      </dl>
      <dl>
        <dt>
          運営からのお知らせ <em>必須</em>
        </dt>
        <dd>
          <?php
            if($back_flag){
              if($_SESSION['form_value']['user_wish_mail_magazine'] == "1"){
                print('<label class="radio-inline">
                 <input checked="checked" id="user_wish_mail_magazine_1" name="user_wish_mail_magazine" type="radio" value="1"> 受信する
                 </label>
                 <label class="radio-inline">
                   <input id="user_wish_mail_magazine_0" name="user_wish_mail_magazine" type="radio" value="0"> 受信しない
                 </label>');
              }else{
                print('<label class="radio-inline">
                 <input id="user_wish_mail_magazine_1" name="user_wish_mail_magazine" type="radio" value="1"> 受信する
                 </label>
                 <label class="radio-inline">
                   <input checked="checked" id="user_wish_mail_magazine_0" name="user_wish_mail_magazine" type="radio" value="0"> 受信しない
                 </label>');
              }
            }else{
              print('<label class="radio-inline">
                 <input checked="checked" id="user_wish_mail_magazine_1" name="user_wish_mail_magazine" type="radio" value="1"> 受信する
                 </label>
                 <label class="radio-inline">
                   <input id="user_wish_mail_magazine_0" name="user_wish_mail_magazine" type="radio" value="0"> 受信しない
                 </label>');
            }
          ?>
        </dd>
     </dl>

     <button type="submit">確認画面へ</button>
     <!-- </form> -->
    </form>
  </div>
</body>
</html>

<?php
  unset($_SESSION['error_message']);
  unset($_SESSION['form_value']);
?>