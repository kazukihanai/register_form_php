<?php
session_start();

$to_customer = $_SESSION['form_value']['mail'];
$to_master = 'kazuki.hanai@fourier.jp';
$subject_customer = 'ご登録ありがとうございました';
$subject_master = '会員登録の通知';
$message_customer = 'ありがとうございました';

$lastname = "姓：　".$_SESSION['form_value']['lastname'];
$firstname = "名：　".$_SESSION['form_value']['firstname'];
$ln_rb = "せい：　".$_SESSION['form_value']['ln_rb'];
$fn_rb = "めい：　".$_SESSION['form_value']['fn_rb'];
$nickname = "ニックネーム：　".$_SESSION['form_value']['nickname'];
$sei = "性別：　".$_SESSION['form_value']['sei'];
$mail = "メールアドレス：　".$_SESSION['form_value']['mail'];
$pass = "パスワード：　".$_SESSION['form_value']['pass'];
$pass_conf = "パスワード（確認）：　".$_SESSION['form_value']['pass_conf'];
$postcode = "郵便番号：　".$_SESSION['form_value']['postcode'];
$prefecture = "県：　".$_SESSION['form_value']['prefecture'];
$address = "住所：　".$_SESSION['form_value']['address'];
$building = "住所（建物名）：　".$_SESSION['form_value']['building'];
$phonenumber = "電話番号：　".$_SESSION['form_value']['phonenumber'];
$message = "メッセージ：　".$_SESSION['form_value']['message'];

if($_SESSION['form_value']['user_wish_mail_magazine'] == "1")
	$user_wish_mail_magazine = "運営からのお知らせ：　受信する";
else
	$user_wish_mail_magazine = "運営からのお知らせ：　受信しない";

$message_master = $lastname."\n".
					$firstname."\n".
					$ln_rb."\n".
					$fn_rb."\n".
					$nickname."\n".
					$sei."\n".
					$mail."\n".
					$pass."\n".
					$pass_conf."\n".
					$postcode."\n".
					$prefecture."\n".
					$address."\n".
					$building."\n".
					$phonenumber."\n".
					$message."\n".
					$user_wish_mail_magazine."\n";
$headers = 'From: kazuki.hanai@fourier.jp' . "\r\n";
?>
<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>会員登録</title>
  <link href="./style.css" rel="stylesheet" type="text/css">
  <script src="https://zipaddr.github.io/zipaddrx.js" charset="UTF-8"></script>
  <script type="text/javascript" src="script.js"></script>
</head>
<body>
	<div id="all">
		<?php
		if(mail($to_customer, $subject_customer, $message_customer, $headers))
			print "メールを送信しました。";
		mail($to_master, $subject_master, $message_master, $headers);
		unset($_SESSION['form_value']);
		?>

		<a href="index.php">トップページへ戻る</a>
	</div>
</body>
</html>

